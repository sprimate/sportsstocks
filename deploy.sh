#!/bin/bash
echo "$1"
if [ "$1" != "" ]; then
	git add -u
	git commit -a -m "$1"
fi
git push
git push heroku master
heroku ps:scale web=1
heroku open
