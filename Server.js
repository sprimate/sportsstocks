//Imports
var express 	= require('express');
var app 		= express();
var server      = require('http').createServer(app);
var io 		 	= require('socket.io').listen(server);
var shortId 	= require('shortid');
var request = require("request");
var exec = require('child_process').exec;

//App setup
app.set('port', (process.env.PORT || 5001));
app.use(express.static(__dirname + '/public'));
app.get( '/*' , function( req, res, next ) {
    var file = req.params[0];
   res.sendFile( __dirname + '/' + file );
}); 

server.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
  //triggers.Email({to: "sprimate@gmail.com", subject: "testSubject", message: "This is a test message email thing."});
});

//Socket Messages
io.on('connection', function(client) {
  client.on("Joined", function(uid){
    console.log("Mapping client; " + uid);
    if (uid)
    {
      clients[uid] = client;
    }
  })
  client.on("Authenticated", function(result){

      if (result && result.uid)
      {
        clients[result.uid] = client;
        exports.OnUserLogin(result.uid);
      }
    })

    client.on("CreateOrder", function(data){
      console.log("Create Order Received: ", data);
      Orders.createOrder(data, client);
    });
});

module.exports = exports = {
  OnUserLogin: function(uid)
  {
    var userKey = "Users/" + uid;
    Firebase.getFirebaseData(userKey, function(userData){
      if (!userData)
      {
        Firebase.getFirebaseData("Market/newUserStartingCurrency", function(startingCurrency){
          userData = {
            currency: startingCurrency
          }

          Firebase.addToFirebase(userKey,userData, function(success){
            if (success)
            {
              console.log("User Created: " + userKey, userData)
            }
            else
            {
              console.log("Error creating new user");
            }
          });
        });
      }
      else
      {     
        console.log("User Logged on. " + userKey);
      }
    });
  }
}
