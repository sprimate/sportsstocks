require("./serverSideJs/Globals.js")
const process = require('process');

process.chdir(__dirname);
var earningsPayoutFrequencyInDays;
var updateLeagueFrequencyInDays;
var dayInMilliseconds = 1000 * 60 * 60 * 24;
function attemptPayoutEarnings()
{
  Firebase.getFirebaseData("Market", function(market){
    var now = new Date().getTime();
    var differenceForEarnings = now - market.lastEarningsPaidTime;
    var targetDifferenceForEarnings = dayInMilliseconds * market.earningsPayoutFrequencyInDays;

    if (differenceForEarnings > targetDifferenceForEarnings)
    {
      Earnings.payout();
    }
    else
    {
      console.log("Will payout in " +((1-differenceForEarnings / targetDifferenceForEarnings) * market.earningsPayoutFrequencyInDays).round(2) + " days");
      process.exit();
    }
  });
}


function afterLeague()
{
  Orders.attemptFillAllOrders();
  attemptPayoutEarnings();
}

Firebase.init(function(){
  console.log("Firebase Successfully Initialized");
  League.grabSeasonStats(afterLeague);
});