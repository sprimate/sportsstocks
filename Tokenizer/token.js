var web3 = require('Web3');
var ether = parseInt(web3.utils.toWei("1", 'ether'));
var address2 = "0x7BE7313D0fcc6Ca6Ef3e9F0AD4a914a91f4bDD11";
StockToken.deployed().then(function(instance){
 	instance.getMinter().then(function(minterAddress){
 		instance.balanceOf(minterAddress).then(function(minterBalance){
			instance.balanceOf(minterAddress).then(function(newMinterBalance){
				console.log("Minter Balance: " + minterBalance + " -> " + newMinterBalance);
				instance.send(7 * ether, {from: address2}).then(function(tx) {
					console.log("Sent 7: ", tx.tx);
					instance.getBank().then(function(bank){
						console.log("Bank: " + bank);
						instance.balanceOf(address2).then(function(a2Balance){
							console.log("address2 Balance: " + a2Balance);
						});
					});
				});
			});
 		})
 	})
 });