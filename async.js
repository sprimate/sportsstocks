String.prototype.hashCode = function() {
  var hash = 0, i, chr;
  if (this.length === 0) return hash;
  for (i = 0; i < this.length; i++) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

var dict = {};
//var sleep  = require('system-sleep');
var sleep  = require('deasync').sleep;

var childProcess = require('child_process');

function sleep3(milliseconds){
	var secs = milliseconds / 1000;
	childProcess.execSync('sleep ' + secs);
}

function sleepFor( sleepDuration ){
    var now = new Date().getTime();
    while(new Date().getTime() < now + sleepDuration){ /* do nothing */ } 
}
//var sleep = require('sleep').msleep;

function sleep2(time) 
{
    var targetDate = new Date(new Date().getTime() + time);
    while(targetDate > new Date())
	{	  
		process.nextTick(function () 
		{ 
			console.log("NextTick")
		});
	}
    console.log("Done");
}

var sleepRate = 600;
var exports = module.exports ={
	init: function(id){
		var suffix = 1;

		while (dict[id + suffix.toString()] != null)
		{
			//console.log("It's " + JSON.stringify(dict[id+suffix.toString()]) + " - " + new Error().stack );
			suffix++;
//			sleep(sleepRate);
		}
		id = id + suffix;
		var defaultVal = new Date().getTime();
		dict[id] = {"defaultVal": defaultVal, "val": defaultVal};
		return id;
	},
	set: function(id, value){
		if (dict[id] == null)
		{
			console.log("async cannot set key '" + id + "' - It does not exist. Has it been initialized? " + new Error().stack);
		}
		dict[id].val = value;
	},
	wait: function(id){
		if (dict[id] == null)
		{
			console.log("async cannot wait for key '" + id + "' - has it been initialized?");
			return null;
		}
		if (dict[id].locked)
		{
			console.log("async cannot wait for key '" + id + "' - it is already being waited for (it's locked)");
			return null;
		}

		dict[id].locked = true;
		var i = 0;
		//return exports.wrap(this, exports.wrappedWait, id, 1);
		
		while(dict[id].val == dict[id].defaultVal)
		{
	    	sleep(sleepRate);
		}

		var ret = dict[id].val;
		dict[id] = null;
		return ret;
	},
	wrappedWait: function(id, callback){
		//console.log("Callback: " + callback);
		if (dict[id].val == dict[id].defaultVal)
		{
			setTimeout(function(){
				exports.wrappedWait(id, callback);
			}, sleepRate);
		}
		else
		{
			var ret = dict[id].val;
			dict[id] = null;
			callback(ret);
		}
	},
	wrap: function(context, f){ //function, all Arguments (Supply all of the arguments but ignore the callback), index of the callback argument
		
		id = exports.init(f.toString().hashCode);
		var waitFunction = function(){
			if (arguments.length > 1)
			{
				exports.set(id, arguments);
			}
			else if (arguments.length > 0)
			{
				 exports.set(id, arguments[0]);
			}
			else
			{
				exports.set(id, null);
			}
		}
		var args = [];
		var pushedCallback = false;
		for (var i = 2; i < arguments.length-1; i++)
		{
			args.push(arguments[i]);
			if (i == arguments[arguments.length-1])
			{
				args.push(waitFunction);
				pushedCallback = true;
			}
		}


		if (!pushedCallback)
		{
			args.push(waitFunction);
		}

		//args.splice(indexOfCallback, 0, waitFunction).join();
		f.apply(context, args);
		return exports.wait(id);		
	}
}

