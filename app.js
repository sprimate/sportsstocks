require("./serverSideJs/Globals.js")
global.Server = require("./Server.js");


var earningsPayoutFrequencyInDays;
var updateLeagueFrequencyInDays;
var dayInMilliseconds = 1000 * 60 * 60 * 24;
Firebase.init(function(){
  console.log("Firebase Successfully Initialized");
  Firebase.getFirebaseData("Market", function(market)
    {
      updateLeagueFrequencyInDays = market.updateLeagueFrequencyInDays;
      if (process.argv.length > 2)
      {
          var arg = process.argv[2].toLowerCase();
          if (arg == "reset")
          {
            console.log("Resetting Firebase");
            Firebase.addToFirebase("Market/bank", 0);
            Firebase.addToFirebase("Market/lastEarningsPaidTime", 0);
            Firebase.addToFirebase("Market/earningsPaid", 0);
            Firebase.addToFirebase("Market/earningsPayoutFrequencyInDays", 7);
            Firebase.addToFirebase("Market/earningsResetDate", new Date("10/15/2020").getTime());
            Firebase.deleteFirebaseKey("Users");
            Firebase.deleteFirebaseKey("Orders");
            grabStats(true);
           return;
         }
         else if (arg == 'payout')
         {
            Earnings.payout();
            return;
         }
         else if (arg == "update")
         {
           grabStats();
           return;
         }
         else if (arg == "ai")
         {
            var maxinstances = process.argv.length > 3 ? process.argv[3] : 30;
            for(var i = 0; i < maxinstances; i++)
            {
              require("./serverSideJs/AI.js").init();
            }
            return;
         }
      }
      var now = new Date().getTime();
      var differenceForLeague = now - market.lastUpdateLeagueTime;
      var targetDifferenceFoLeague = dayInMilliseconds * updateLeagueFrequencyInDays;

      if (differenceForLeague > targetDifferenceFoLeague)
      {
        grabStats();
      }
      else
      {
        //console.log(difference + "/" + targetDifference);
        console.log("Updating league in " + ((1-differenceForLeague / targetDifferenceFoLeague) * updateLeagueFrequencyInDays * 24).round(2)+ " hours");
        setTimeout(grabStats, targetDifferenceFoLeague);
        afterLeague();
      }
  });
});

function attemptPayoutEarnings()
{
  Firebase.getFirebaseData("Market", function(market){
    var now = new Date().getTime();
    var differenceForEarnings = now - market.lastEarningsPaidTime;
    var targetDifferenceForEarnings = dayInMilliseconds * market.earningsPayoutFrequencyInDays;

    if (differenceForEarnings > targetDifferenceForEarnings)
    {
      Earnings.payout();
    }
    else
    {
      console.log("Will payout in " +((1-differenceForEarnings / targetDifferenceForEarnings) * market.earningsPayoutFrequencyInDays).round(2) + " days");
    }
  });
}

function grabStats(reset){
  League.grabSeasonStats(afterLeague, reset);
  setInterval(grabStats, dayInMilliseconds * updateLeagueFrequencyInDays);
}

function afterLeague()
{
 // Orders.attemptFillAllOrders();
  attemptPayoutEarnings();
}