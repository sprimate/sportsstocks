module.exports = exports = {
	payout: function()
	{
		var dateKey = getDateKey();
		Firebase.getFirebaseData("Market", function(market){			
			//This date represents the 1 year mark.
			var resetDate = new Date(market.earningsResetDate);

			var targetTime = 1 / (market.targetDividendYield/100);
			var toAdd = targetTime >= 1 ? targetTime - 1 : -(1-targetTime);
			toAdd *= 365.25;//how many days to add or subtract to the target date to match the target dividend yield.
			resetDate.setDate(resetDate.getDate() + toAdd);

			var nowDate = new Date();
			var differenceInDays = (resetDate - nowDate) / (1000 * 3600 * 24); 
			var payoutsLeftBeforeReset = Math.floor(differenceInDays / market.earningsPayoutFrequencyInDays)
			if (payoutsLeftBeforeReset <= 0)
			{
				console.log("No more payouts scheduled until new earningsResetDate occurs");
				return;
			}
			var payout = (market.bank /payoutsLeftBeforeReset).floor(2);
			console.log("Payouts Left Until Reset: " + payoutsLeftBeforeReset);
			console.log("current Payout: " + payout);
			Firebase.addToFirebase("Market/lastEarningsPaidTime", nowDate.getTime());
			var nextDate = new Date();
			nextDate.setDate(nextDate.getDate() + market.earningsPayoutFrequencyInDays);
			Firebase.addToFirebase("Market/nextEarningsPaidTime", nextDate.getTime());
			Firebase.getFirebaseData("League", function(league)
			{
				Object.keys(league).forEach(function(name){
					if (league[name].PotentialDividends)
					{
						league[name].PotentialDividends = 0;
					}

					league[name].PotentialDividends += (payout * Math.max(0, parseFloat(league[name].FantasyPointsPercentage)/100));
					//console.log(name + ": ", league[name].PotentialDividends);
				});

				Firebase.addToFirebase("League", league);
				Firebase.actOnFirebaseDbRef("Users", function(usersRef)
				{
					usersRef.orderByChild("Stocks").startAt("a").once("value", function(snapshot){
						var users = snapshot.val();
						if (users == null)
						{
							return;
						}
						var userIds = Object.keys(users);
						if (userIds.length > 0)
						{
							Firebase.createFirebaseTransaction("Market/bank", -payout, null, function(moneyRetrievedFromBank, err){
								Firebase.createFirebaseTransaction("Market/earningsPaid", payout);
								if (!moneyRetrievedFromBank){
									console.log("Couldn't retrieve [$" + payout + "] from bank: " + err);
									return;
								}
								else
								{

									var totalScore = 0;
									var playersDict = {};
									userIds.forEach(function(userId){
										users[userId].totalEarningsToUpdate = 0;
										var user = users[userId];
										Object.keys(user.Stocks).forEach(function(userPlayer){
											if (!playersDict[userPlayer])
											{
												playersDict[userPlayer] = {
													userIds: {}
												};

												if (market.useYearlyAverageForDividends)
												{
													playersDict[userPlayer].score = league[userPlayer].AdjustedFantasyScore;
												}
												else
												{
													var scores = league[userPlayer].FantasyPointsHistory;
													var scoresArray = [];
													for (var dateKey in scores)
													{
														scoresArray.push({
															date: getDateFromDateKey(dateKey),
															value: scores[dateKey]
														});
													}

													scoresArray.sort(function(a, b){return b-a});
													var earliestToCheck = getDateFromToday();
													earliestToCheck.setDate(earliestToCheck.getDate() - market.earningsPayoutFrequencyInDays);
													var total = 0;
													for (var i in scoresArray)
													{
														if (scoresArray[i].date > earliestToCheck)
														{
															total += scoresArray[i].value;
														}
														else
														{
															break;
														}
													}

													playersDict[userPlayer].score = total;
												}
											}

											//a player that owns 2 stocks @ 25% gets paid the same as a player who owns 1 stock at 50%
											totalScore += playersDict[userPlayer].score * user.Stocks[userPlayer].Owned;
											playersDict[userPlayer].userIds[userId] = user.Stocks[userPlayer].Owned;
										})
									})

									var playerNames = Object.keys(playersDict)
									playerNames.forEach(function(playerName){
										var player = playersDict[playerName];
										var playerUserIds = Object.keys(player.userIds);
										var totalOwnedStock = 0;
										playerUserIds.forEach(function(userIdToUpdate){
											totalOwnedStock += player.userIds[userIdToUpdate];
											//players fantasy score * amount of stock this player owns
											var score = player.score * player.userIds[userIdToUpdate];
											wage = (score/totalScore * payout).floor(2);
											var wagePerShare = wage / player.userIds[userIdToUpdate];
											//console.log(userIdToUpdate + " receives " + wage + " for " + playerName + "'s " + (score/totalScore*100) + "% share of " + payout);
											Firebase.createFirebaseTransaction("Users/" + userIdToUpdate + "/Dividends/" + playerName + "/Total", wage);//, null, function(playerEarningsSuccess){
											Firebase.createFirebaseTransaction("Users/" + userIdToUpdate + "/Dividends/" + playerName + "/PerShare", wagePerShare);//, null, function(playerEarningsSuccess){
											Firebase.addToFirebase("Users/" + userIdToUpdate + "/Dividends/" + playerName + "/" + dateKey, {
												Dividends: wage,
												DividendsPerShare: wagePerShare
											});//, null, function(playerEarningsSuccess){
											logPlayerWages(wage, userIdToUpdate, playerName);
											users[userIdToUpdate].totalEarningsToUpdate += wage;
										})
										wage = (player.score * totalOwnedStock/totalScore * payout).floor(2);
										Firebase.createFirebaseTransaction("League/" + playerName + "/Dividends", wage);
										Firebase.createFirebaseTransaction("League/" + playerName + "/DividendsPerShare", wage/totalOwnedStock)
									});

									userIds.forEach(function(finalUserId){
										payToUser(users[finalUserId].totalEarningsToUpdate, finalUserId);
									})
								}
							});
						}
					})
				});
			});
		});
	}
}

function logPlayerWages(wage, userId, playerName)
{

}	

function payToUser(wage, userId){
	//console.log("User " + userId + " gets " + wage);
	Firebase.createFirebaseTransaction("Users/" + userId + "/totalEarnings", wage, null, function(userPaid, message){
		if (!userPaid)
		{
			console.log("User "  + userId + "couldn't be paid. Sending money back to bank");
			Firebase.createFirebaseTransaction("Market/bank", wage);
			Firebase.createFirebaseTransaction("Market/earningsPaid", -wage);
			Firebase.pushToFirebase('Market/disruptedEarningsPayments', {
				amount: wage,
				user: userId,
				message: message
			});
		}
	});
	Orders.addCurrency(userId, wage);
}