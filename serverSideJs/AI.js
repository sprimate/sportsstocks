var maxTimeBeforeNextAction = 15;//minutes
var actions = [ //frequncies help determinne how often certain funnctions are called.
	limitBuy, limitBuy, 
	limitSell, limitSell, 
	marketBuy, marketBuy, marketBuy,
	marketSell, marketSell, 
 	leagueBuy, leagueBuy, leagueBuy, 
 	cancelOrder
 	];

global.activeAis = 0;
module.exports = exports = {
	init: function()
	{
		return new AI();
	}
}

class AI{
	constructor(){
			activeAis++;
			this.userId = "AI" + activeAis;
			Server.OnUserLogin(this.userId);
			this.act();
	}

	act(){
		var userId = this.userId;
		var context = this;	
		var timeBeforeNextAction = Math.random()*maxTimeBeforeNextAction * 60 * 1000;
		setTimeout(function(){
			actions[random(0, actions.length-1)](userId)
			context.act()
		}, timeBeforeNextAction);
		
	}
}

function fillOrder(userId)
{
	//get list of limit sells, pick one, and fill some or all of it
	Firebase.getFirebaseData("Orders", function(orders){
		if (orders)
		{
			var keys = shuffle(Object.keys(orders));
			while (keys.length > 0)
			{
				var order = orders[keys[0]];
				if (order.userId == userId || order.orderType != "LimitSell")
				{
					keys.shift();
				}
				else
				{
					var data = {
						userId: userId,
						amountOfStock: Math.floor(Math.random() * (order.amountOfStock))+1,
						stockId: order.stockId,
						stockPrice: order.stockPrice,
						orderType: "LimitBuy"
					}

					console.log("[" + userId + "] Creating order: " , data);
					Orders.createOrder(data);
					break;
				}
			}
		}
	});
}

function limitBuy(userId){
	Firebase.getFirebaseData("League", function(league){
		if (league)
		{
			var keys = Object.keys(league);
			var data = {
				userId: userId,
				amountOfStock: random(1, 50),
				stockId:  keys[random(0, keys.length-1)],
				orderType: "LimitBuy"
			}

			League.getAskPrice(data.stockId, function(ogPrice){
				data.stockPrice = ogPrice + ogPrice * randomFloat(-0.15, .15);
				console.log("[" + userId + "] with og Price [" + ogPrice + "] " + " Creating order: " , data);
				Orders.createOrder(data);
			});
		}
	});
}

function limitSell(userId)
{
	//pick a owned stoxk and limit sell some or all of it (set the price randomly about 15% around the league price)
	Firebase.getFirebaseData("Users/" + userId + "/Stocks", function(stocks){
		if (!stocks)
		{
			return;
		}
		var keys = Object.keys(stocks);
		var data = {
			userId: userId,
			stockId:  keys[random(0, keys.length-1)],
			orderType: "LimitSell"
		}

		var stock = stocks[data.stockId];
		if (stock.Owned == 0)
		{
			return;
		}

		data.amountOfStock = random(1, stock.Owned);
		League.getAskPrice(data.stockId, function(ogPrice){
			data.stockPrice = ogPrice + ogPrice * randomFloat(-0.20, .20);
			Orders.createOrder(data);
		});
	});
}

function marketBuy(userId)
{
	//pick a random stock ad market buy a random amount of it
	Firebase.getFirebaseData("League", function(league){
		if (league)
		{
			var keys = Object.keys(league);
			var data = {
				userId: userId,
				amountOfStock: random(1, 50),
				stockId:  keys[random(0, keys.length-1)],
				orderType: "MarketBuy"
			}

			Orders.createOrder(data);
		}
	});
}

function marketSell(userId)
{
	//pick a random stock and market sell a random amount of it
	Firebase.getFirebaseData("Users/" + userId + "/Stocks", function(stocks){
		if (!stocks)
		{
			return;
		}
		var keys = Object.keys(stocks);
		var data = {
			userId: userId,
			stockId:  keys[random(0, keys.length-1)],
			orderType: "MarketSell"
		}

		var stock = stocks[data.stockId];
		if (stock.Owned == 0)
		{
			return;
		}

		data.amountOfStock = random(1, stock.Owned);
		Orders.createOrder(data);
	});
}

function leagueBuy(userId)
{

	//pick a random stock and league buy a random amount of it
	Firebase.getFirebaseData("League", function(league){
		if (league)
		{
			var keys = Object.keys(league);
			var data = {
				userId: userId,
				amountOfStock: random(1, 50),
				stockId:  keys[random(0, keys.length-1)],
				orderType: "LeagueBuy"
			}

			League.getAskPrice(data.stockId, function(leaguePrice){
				data.stockPrice = leaguePrice
				Orders.createOrder(data);
			});
		}
	});
}

function cancelOrder(userId)
{
	//pick an active limit order and cancel it
	Firebase.getFilteredFirebaseData("Orders", "userId", userId, function(orders){
		if (orders && orders.length > 0)
		{
			var orderToCancel = orders[random(0, orders.length-1)];
			Orders.cancelOrder(orderToCancel, "Cancelled by AI");
		}
	})
}