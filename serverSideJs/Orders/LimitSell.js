module.exports = exports = {
    fill: function (order, callback)
    {
      criterias = [{
          childPropertyName: "orderType",
          operator: "==",
          compareValue: "LimitBuy"
        },
        {
          childPropertyName: "stockPrice",
          operator: ">=",
          compareValue: order.stockPrice
        },
        {
          childPropertyName: "userId",
          operator: "!=",
          compareValue: order.userId
        }
      ];

      Orders.getOrdersByCriteria(order.stockId, criterias, function (limitBuyOrders) {
              if (!limitBuyOrders || limitBuyOrders.length == 0) {
                callback(order, false, "No Buys are currently available to fill this order.");
              } else {
                limitBuyOrders.sort(function (a, b) {
                  return a.timestamp > b.timestamp //Retrieve the earliest created order that fulfills the sale.
                });

                Orders[limitBuyOrders[0].orderType].fill(limitBuyOrders[0], function(buyOrder, success, msg){
                  callback(order, success, msg);
                });
              }
            });
    },
    isCreationValid: function(order, callback){
      Firebase.getFirebaseData("Users/" + order.userId + "/Stocks/" + order.stockId + "/Owned", function(playerStocks){
        if (playerStocks < order.amountOfStock)
        {
          callback("Insufficient Stock Availability");
        }
        else
        {
          callback();
        }
      });
    }
  }