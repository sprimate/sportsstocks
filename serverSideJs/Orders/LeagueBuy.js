module.exports = exports = {
    fill: function(order, finalCallback){
      var userId = order.userId;
      var amountOfStock = order.sellOrder ? order.sellOrder.amountOfStock : order.amountOfStock;

      var stockId = order.stockId;
      var callback = function(success, errMessage){
        if (finalCallback)
        {
          finalCallback(order, success, errMessage);
        }
      };

     // console.log("Filling LeagueBuy Order: ", order);
      League.getAskPrice(order.stockId, function(stockPrice){
	      //callback = callback ? callback : function(){};
	      var amountToSpend = stockPrice * amountOfStock
	     // console.log("Removing  "+ amountToSpend);

	      Orders.addCurrency(userId, -amountToSpend, function(success, err){
	        console.log("Add currency completed. Success? " + success);
	        if (success)
	        {
	         // console.log("Removing stock from League/" + stockId);
	          Firebase.createFirebaseTransaction("League/" + stockId+ "/Stocks", -amountOfStock, function(currentStockAmount){
	          	console.log(currentStockAmount + " - " + amountOfStock + " >= " + 0);
	              return currentStockAmount - amountOfStock >= 0
	            }, function(success2, err2){
	              //console.log("Remove stock completed. Success? " + success2);
	              if (success2)
	              {
	               // console.log("Adding stock to user " + userId);
	                Firebase.createFirebaseTransaction("Users/" + userId + "/Stocks/" + stockId + "/Owned", amountOfStock, null, function(stockIncreaseSuccess, stockIncreaseError){
	                	if (stockIncreaseSuccess)
	                	{
	                		if (order.sellOrder && order.amountOfStock - order.sellOrder.amountOfStock > 0)
	                		{
	                			var newOrder = {
	                				userId: order.userId,
	                				amountOfStock: order.amountOfStock - order.sellOrder.amountOfStock,
	                				stockId: order.stockId,
	                				orderType: "MarketBuy",
	                				createdFrom: order.id
	                			}
	                			Orders.saveOrder(newOrder, function (newCreatedOrder, newOrderSynced, newOrderSyncError) {
	                				if (newOrderSynced)
	                				{
	                					callback(true);
	                				}
	                				else
	                				{
	                					callback(false, newOrderSyncError )
	                				}
	                			});
	                		}
	                		else
	                		{
	                			callback(true);
	                		}
	                	}
	                	else
	                	{
	                		callback(false, stockIncreaseError);
	                	}
	                });
	                Firebase.createFirebaseTransaction("Market/bank", amountToSpend, null, function(bankSuccess, bankError){
	                  if (!bankSuccess)
	                  {
	                    console.log("Error saving [" + amountToSpend + "] bank funds: ", bankError);
	                  }
	                });
	              }
	              else
	              {
	                Orders.addCurrency(userId, amountToSpend, function(success3, err3){
	                  if (success)
	                  {
	                    if (!err2)
	                    {
	                      callback(false, "Insufficient stock availability");
	                    }
	                    else
	                    {
	                      callback(false, err2);
	                    }
	                  }
	                  else
	                  {
	                    callback(false, err2 + "\nError refunding $" + amountToSpend + " to player " + userId);
	                  }
	                })
	              }
	            });
	        }
	        else
	        {
	          if (!err)
	          {
	            callback(false, "Insufficient Funds.");
	          }else
	          {
	            callback(false, err);
	          }
	        }
        })

      });
    },
    isCreationValid: function(order, callback){
      Firebase.getFirebaseData("League/" + order.stockId + "/Stocks", function(stocks){
        if (stocks < order.amountOfStock)
        {
          callback("Insufficient Stock Availability");
        }
        else
        {
          Firebase.getFirebaseData("Users/" + order.userId + "/currency", function(currency){
            League.getAskPrice(order.stockId, function(askPrice){
              if (currency <  askPrice * order.amountOfStock)
              {
                callback("Insufficient Funds");
              }
              else
              {
                callback();
              }
            })
          });
        }
      })
    }
  }