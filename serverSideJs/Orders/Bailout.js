module.exports = exports = {
    fill: function (order, finalCallback)
    {
      var callback = function(s, e){
        if (finalCallback)
        {
          finalCallback(order, s, e);
        }
      }
      console.log("Executing Bailout Request: ", order);
      Firebase.getFirebaseData("Users/" + order.userId + "/Stocks/" + order.stockId + "/Owned", function(playerStocks){
        if (playerStocks < order.amountOfStock)
        {
          callback(false, "Insufficient stock availability");
        }
        else
        {
          Firebase.getFirebaseData("League/" + order.stockId, function(player){
            Firebase.getFirebaseData("Market", function(market){
              League.getBidPrice(order.stockId, function(bidPrice)
              {
                var price = Math.min(market.bank, bidPrice * order.amountOfStock);
                //If the price isn't the same as the requested price, double check with the user, while freezing the price at the askPrice.
                Firebase.createFirebaseTransaction("Market/bank", -price, function(bank){
                  return bank - price >= 0;
                }, function(bankSuccess, bankError){
                  if (bankSuccess)
                  {
                    Firebase.createFirebaseTransaction("Users/" + order.userId + "/currency", price, null, function(currencySuccess, currencyError){
                      if (!currencySuccess)
                      {
                          callback(false, "Error with paying user [" + order.userId + "] it's owed currency [$" + price + "]");
                      }
                      else
                      {
                        Firebase.createFirebaseTransaction("Users/" + order.userId + "/Stocks/" + order.stockId + "/Owned", -order.amountOfStock, function(currentAmountOfStock){
                          return currentAmountOfStock - order.amountOfStock >= 0;
                        }, function(userStockSuccess, userStockError){
                          if (userStockSuccess)
                          {
                            Firebase.createFirebaseTransaction("League/" + order.stockId + "/Stocks", order.amountOfStock, function(currentAmountOfLeagueStock){
                              return currentAmountOfLeagueStock <= market.numSharesPerEntity;
                            }, function(replenishStockSuccess, replenishStockError){
                              if (replenishStockSuccess)
                              {
                                Firebase.getFirebaseData("Users/" + order.userId + "/Stocks/" + order.stockId + "/Owned", function(stillOwned){
                                  if (stillOwned < 0)
                                  {
                                    callback(false, 'Somehow removed too many stock from this player? please investigate');
                                  }
                                  else if (stillOwned == 0)
                                  {
                                    Firebase.deleteFirebaseKey("Users/" + order.userId + "/Stocks/" + order.stockId + "/Owned");
                                    callback(true);
                                  }
                                  else
                                  {
                                    callback(true);
                                  }
                                })
                              }
                              else
                              {
                                callback(false, "Unable to replenish stock to league. " + replenishStockError);
                              }
                            });
                          }
                          else
                          {
                            callback(false, "Unable to validate user stock amount. " + userStockError);
                          }
                        });
                      }
                    });
                  }
                  else
                  {
                    callback(false, "Error with chosing bailout price. Please try again.")
                  }
                });
              });
            });
          });
        }
      });
    },
    isCreationValid: function(order, callback){
      Firebase.getFirebaseData("Users/" + order.userId + "/Stocks/" + order.stockId + "/Owned", function(ownedStock){
        console.log(order.stockId + " Owned: " + ownedStock + " < " + order.amountOfStock);
        if (ownedStock < order.amountOfStock)
        {
          callback("Insufficient Stock Availability");
        }
        else
        {
          callback();
        }
      })
    }
  }