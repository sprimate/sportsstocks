module.exports = exports = {
    fill: function (order, callback)
    {
    	var criteria = [{
    		childPropertyName: "orderType",
    		operator: "==",
    		compareValue: "LimitSell"
    	},
    	{
    		childPropertyName: "userId",
    		operator: "!=",
    		compareValue: order.userId
    	}]

    	console.log("Try fill MarketBuy: ", Orders.getShortOrderString(order, true));
    	Orders.getOrdersByCriteria(order.stockId, criteria, function(limitSellOrders){
          League.getAskPrice(order.stockId, function(askPrice){
            Firebase.getFirebaseData("League/" + order.stockId + "/Stocks", function(leagueStock){
        	  var stockToBuy = order.amountOfStock;
        	  if (leagueStock > 0)
        	  {
	              limitSellOrders.push({
	                amountOfStock: Math.min(leagueStock, stockToBuy),
	                stockPrice: askPrice,
	                orderType: "LeagueSell"
	              });
	          }

              limitSellOrders.sort(function(a, b){ return a.stockPrice > b.stockPrice});
              if (limitSellOrders.length == 0)//If no orders available, cancel the market buy!
              {
              	Orders.cancelOrder(order, "Lack of liquidity for [" + order.stockId + "]", function(canceledSuccess){
              		if (canceledSuccess)
              		{
              			callback(order, false, "Order Canceled due to lack of liquidity for [" + order.stockId + "]");
              		}
              		else
              		{
              			callback(newOrder, false, "Failed to cancel order [" + newOrder.id + "]: " + newOrderPurchaseError);
              		}
              	});
              	
              	return;
              }

              var sellOrder = limitSellOrders[0];
              //always complete limits before league if they're the same price
              if (sellOrder.orderType == "LeagueSell" && limitSellOrders.length > 1 && sellOrder.stockPrice == limitSellOrders[1].stockPrice)
              {
                sellOrder = limitSellOrders[1];
              }
              if (stockToBuy > 0)
              {
                order.sellOrder = sellOrder;
                var adjustedOrder = cloneDeep(order);
                adjustedOrder.orderType = sellOrder.orderType == 'LeagueSell' ? "LeagueBuy" : "LimitBuy";

                Orders.attemptFillOrder(adjustedOrder,function(filledOrder, buySuccess, buyError){
                	callback(filledOrder, buySuccess, buyError);
                	callback(order, buySuccess, buyError);
                  /*if (buySuccess)
                  {
                    if (filledOrder.filled)
                    {
                      Firebase.getFirebaseData("Orders/" + filledOrder.filled.newOrderId, function(newOrder){
                      	newOrder.orderType = "MarketBuy";
                        Orders.attemptFillOrder(newOrder, function(newOrderFilled, newOrderPurchaseError){
                        	if (newOrderFilled)
                        	{
                        		callback(newOrder, true);
                        	}
                        	else
                        	{
								console.log("wut");
	                        }
                        });
                      });
                    }
                    else
                    {
                    	callback(order, true);
                    }
                  } 
                  else
                  {
                  	callback(order, false, "Failed to buy stock: " + buyError);
                  }*/
                });
              }
            })
        });
      })
    },
    isCreationValid: function(order, callback){
      Firebase.getFirebaseData("Market/numSharesPerEntity", function(stocks){
        if (stocks < order.amountOfStock)
        {
          callback("Insufficient Stock Availability");
        }
        else
        {
          callback();
        }
      });
    }
  }