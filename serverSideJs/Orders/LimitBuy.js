module.exports = exports = {
	fill: function (order, callback) {
		var sellOrder = order.sellOrder;
		if (!sellOrder) {
			criterias = [{
					childPropertyName: "orderType",
					operator: "==",
					compareValue: "LimitSell"
				},
				{
					childPropertyName: "stockPrice",
					operator: "<=",
					compareValue: order.stockPrice
				},
				{
					childPropertyName: "userId",
					operator: "!=",
					compareValue: order.userId
				}
			];
			return Orders.getOrdersByCriteria(order.stockId, criterias, function (limitSellOrders) {
				if (!limitSellOrders || limitSellOrders.length == 0) {
					callback(order, false, "No Sells are currently available to fill this order.");
				} else {
					//get the cheapest price available
					limitSellOrders.sort(function (a, b) {
						return a.stockPrice > b.stockPrice
					});
					order.sellOrder = limitSellOrders[0];
					exports.fill(order, callback);
				}
			});
		}
		if (order.stockId != sellOrder.stockId) {
			return (callback(order, false, "Stock Ids don't match the buy [" + order.stockId + "] and sell [" + sellOrder.stockId + "] orders."))
		}
		var stockToPurchase = Math.min(order.amountOfStock, sellOrder.amountOfStock);
		var price = stockToPurchase * sellOrder.stockPrice;
		Orders.addCurrency(order.userId, -price, function (reducePriceSuccess, reducePriceErr) {
			if (!reducePriceSuccess) {
				if (!reducePriceErr) {
					callback(order, false, "Insufficient Funds.");
				} else {
					callback(order, false, err);
				}
			} else {
				Firebase.createFirebaseTransaction("Users/" + sellOrder.userId + "/Stocks/" + order.stockId + "/Owned", -stockToPurchase, function (currentStockAmount) {
					return currentStockAmount - stockToPurchase >= 0;

				}, function (reduceStockSuccess, reduceStockErr) {
					if (!reduceStockSuccess) {
						Orders.addCurrency(order.userId, price, function (refundSuccess, refundErr) {
							if (refundSuccess) {
								if (!reduceStockErr) {
									callback(order, false, "Insufficient stock availability");
								} else {
									callback(order, false, reduceStockErr);
								}
							} else {
								callback(order, false, err2 + "\nError refunding $" + price + " to player " + userId + ": " + refundErr);
							}
						})
					} else {
						// console.log("Adding stock to user " + userId);
						Firebase.createFirebaseTransaction("Users/" + order.userId + "/Stocks/" + order.stockId + "/Owned", stockToPurchase, null, function (ownedStockSuccess, ownedStockError) {
							if (!ownedStockSuccess) {
								callback(order, false, "Error assigning [" + order.amountOfStock + "] to user " + order.userId + ": " + ownedStockError);
							} else {
								Orders.addCurrency(sellOrder.userId, price, function (paymentSuccessful, paymentErr) {
									if (!paymentSuccessful) {
										callback(order, false, "Error paying [$" + price + "] to user [" + sellOrder.userId + "]");
									} else {
										//console.log("Finalizing LimitBuy: ", order);
										if (sellOrder.amountOfStock == order.amountOfStock) //complete both orders
										{
											console.log("Completing both orders");
											Orders.logCompletedOrder(order.sellOrder, function(completedSellOrderErr){
												if (completedSellOrderErr)
												{
													callback(order, false, "Failed to complete sell order: " + completedSellOrderErr);
												}
												else
												{
													callback(order, true);			
												}
											});
										} else if (sellOrder.amountOfStock < order.amountOfStock) //Complete sell order, make new buy order
										{
											console.log("Completing sell order, updating buy order");

											//Complete sell order
											Orders.logCompletedOrder(order.sellOrder, function (completedSellOrderErr) {
												if (completedSellOrderErr) {
													callback(order, false, completedSellOrderErr);
												} else {
													var newOrder = {
														userId: order.userId,
														amountOfStock: order.amountOfStock - stockToPurchase,
														stockId: order.stockId,
														stockPrice: order.stockPrice,
														orderType: order.orderType,
														createdFrom: order.id
													}
													//Make new Buy Order
													Orders.saveOrder(newOrder, function (newCreatedOrder, newOrderSynced) {
														if (newOrderSynced) {
															order.filled = {
																newOrderId: newCreatedOrder.id,
																filledAmount: stockToPurchase,
																filledByOrderid: sellOrder.id,
																filledStockPrice: sellOrder.stockPrice,
																timestamp: timestamp()
															};
															
															//update old buy order with info about what was filled
															Firebase.addToFirebase("Orders/" + order.id + "/filled", order.filled, function (orderUpdated) {
																if (orderUpdated) {
																	callback(order, true)
																	exports.fill(newCreatedOrder, callback);
																} else {
																	callback(order, false, "Failed to update partially completed order [" + order.id + "] with filled [" + JSON.stringify(filled, null, 2) + "] information")
																}
															});
														} else {
															callback(order, false, "Failed to save new order [" + JSON.stringify(newOrder, null, 2) + "] after [" + order.id + "] was partially filled.");
														}
													});
												}
											});
										} else if (sellOrder.amountOfStock > order.amountOfStock) //complete buy order, make new sell order
										{
											console.log("Completing buy order, updating sell order");
											callback(order, true);

											//Make new Sell Order
											var newOrder = {
												userId: sellOrder.userId,
												amountOfStock: sellOrder.amountOfStock - stockToPurchase,
												stockId: sellOrder.stockId,
												stockPrice: sellOrder.stockPrice,
												orderType: sellOrder.orderType,
												createdFrom: sellOrder.id
											}
											Orders.saveOrder(newOrder, function (newCreatedOrder, newOrderSynced) {
												if (newOrderSynced) {
													sellOrder.filled = {
														newOrderId: newCreatedOrder.id,
														filledAmount: stockToPurchase,
														filledByOrderId: order.id,
														filledStockPrice: sellOrder.stockPrice,
														timestamp: timestamp()
													};
													//updating sell order with filled info, for tracing functionality
													Firebase.addToFirebase("Orders/" + sellOrder.id + "/filled", sellOrder.filled, function (orderUpdated) {
														if (orderUpdated) {
															//complete old sell order
															console.log("Order should be updated.");
															Orders.logCompletedOrder(sellOrder, function (completedSellOrderErr) {
																if (completedSellOrderErr) {
																	callback(order, false, completedSellOrderErr);
																} else {
																	//callback(order, true);
																	//Orders.attemptFillOrder(newCreatedOrder);
																}
															});
														} else {
															callback(order, false, "Failed to update partially completed order [" + sellOrder.id + "] with filled [" + JSON.stringify(filled, null, 2) + "] information");
														}
													});
												} else {
													callback(order, false, "Failed to save new order [" + JSON.stringify(newOrder, null, 2) + "] after [" + sellOrder.id + "] was partially filled.");
												}
											});
										}
										else
										{
											callback(order, false, "Unknown Error: Orders not properly updated for unknown reasons");
										}
									}
								})
							}
						});
					}
				});
			}
		});
	},
	isCreationValid: function (order, callback) {
		Firebase.getFirebaseData("Market/numSharesPerEntity", function (stocks) {
			if (stocks < order.amountOfStock) {
				callback("Insufficient Stock Availability");
			} else {
				callback();
			}
		});
	}
}