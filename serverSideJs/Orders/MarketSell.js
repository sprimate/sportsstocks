module.exports = exports = {
    fill: function (order, callback)
    {
      var criteria = [{
        childPropertyName: "orderType",
        operator: "==",
        compareValue: "LimitBuy"
      },
      {
        childPropertyName: "userId",
        operator: "!=",
        compareValue: order.userId
      }]

      Orders.getOrdersByCriteria(order.stockId, criteria, function(limitBuyOrders){
          League.getBidPrice(order.stockId, function(bidPrice){
              limitBuyOrders.push({
                amountOfStock: order.amountOfStock,
                stockPrice: bidPrice,
                orderType: "LeagueBuy"
              });

              limitBuyOrders.sort(function(a, b){ return a.stockPrice < b.stockPrice});
              var buyOrder = limitBuyOrders[0];
              //always complete limits before league if they're the same price
              if (buyOrder.orderType == "LeagueBuy" && limitBuyOrders.length > 1 && buyOrder.stockPrice == limitBuyOrders[1].stockPrice)
              {
                buyOrder = limitBuyOrders[1];
              }

              if (order.amountOfStock > 0)
              {
                order.buyOrder = buyOrder;
                var adjustedOrder = cloneDeep(order);
                adjustedOrder.orderType = buyOrder.orderType == 'LeagueBuy' ? "Bailout" : "LimitSell";
                Orders.attemptFillOrder(adjustedOrder,function(filledOrder, sellSuccess, sellError){
                  if (sellSuccess)
                  {
                    if (filledOrder.filled)
                    {
                      Firebase.getFirebaseData("Orders/" + filledOrder.filled.newOrderId, function(newOrder){
                        newOrder.orderType = "MarketSell";
                        Orders.attemptFillOrder(newOrder, callback);
                      });
                    }
                    else
                    {
                      callback(order, true);
                    }
                  } 
                  else
                  {
                    callback(order, false, "Failed to sell stock: " + sellError);
                  }
                });
              }
            });
        });
    },
    isCreationValid: function(order, callback){
      Firebase.getFirebaseData("Users/" + order.userId + "/Stocks/" + order.stockId + "/Owned", function(playerStocks){
        if (playerStocks < order.amountOfStock)
        {
          callback("Insufficient Stock Availability");
        }
        else
        {
          callback();
        }
      });
    }
  }