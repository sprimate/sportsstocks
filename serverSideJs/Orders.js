const {JSONPath} = require('jsonpath-plus');

var Firebase;
var League;
module.exports = exports = {
  init: function(firebase, league){
    Firebase = firebase;
    League = league;
  },
  createOrder: function (data)//emits back to client
  {
    console.log("Order Creation Request of type [" + data.orderType + "] Received");
    exports.saveOrder(data, function(order, orderSaved, message)
    {
      var client = clients[order.userId];
      var ret = {data: order, success: orderSaved, message: message};
      console.log("Order Creation Completed: " + orderSaved + "|" + message);

      if (client)
      {
        client.emit("OrderCreationResponse", ret);
      }
      else
      {
        console.log("No client available to receive OrderCreationResponse for: ", ret);
      }
      /*if (orderSaved)
      {
        exports.attemptFillOrder(data);
      }*/
    });         
  },
  saveOrder: function(order, callback)
  {
    order.amountOfStock = parseInt(order.amountOfStock);
    order.stockPrice = parseFloat(order.stockPrice);
    order.timestamp = timestamp();
    var orderType = order.orderType.toLowerCase();
    console.log("Creating new order: " + getShortOrderString(order));
    checkOrderRestrictions(order, function(orderIsValid, err){
      if (orderIsValid)
      {
        Firebase.pushToFirebase("Orders", order, function(orderCreated, key){
          if (orderCreated)
          {
            order.id = key;
            Firebase.addToFirebase("Orders/" + key + "/id", key, function(orderIdAdded){
              if (orderIdAdded)
              {
                Firebase.addToFirebase("Users/" + order.userId + "/ActiveOrders/" + key, getShortOrderString(order), function(userOrderSynced){
                  if (!callback && !userOrderSynced)
                  {
                    console.log("User order not properly synced.")
                  }
                  else if (callback)
                  {
                    callback(order, userOrderSynced);
                    if (userOrderSynced)
                    {
                      exports.attemptFillOrder(order);
                    }
                  }
                });
              }
              else
              {
                console.log("User order ID not properly synced");
              }
            })
          }
          else
          {
            console.log("Order not created");
          }
        });
      }
      else
      {
        callback(order, false, err);
      }
    })  
  },
  cancelOrder: function(order, reasonForCancel, callback)
  {
    console.log("Cancling order " + getShortOrderString(order) + ": " + reasonForCancel);
    callback = callback ? callback : function(){}
    Firebase.getFirebaseData("Orders/" + order.id, function(newestOrder)
    {
      if (!newestOrder)
      {
        callback(false);
        return;
      }
      newestOrder.reasonForCancel = reasonForCancel;
      Firebase.deleteFirebaseKey("Users/" + order.userId + "/ActiveOrders/" + order.id, function(activeOrdersCanceled)
      {
          if (activeOrdersCanceled)
          {
            Firebase.deleteFirebaseKey("Orders/" + order.id, function(deleted)
            {
              if (deleted)
              {
                Firebase.addToFirebase("Users/" + order.userId + "/CanceledOrders/" + order.id, newestOrder, function(orderCancelled){
                  if (orderCancelled)
                  {
                    console.log("Order [" + order.id + "] successfully Canceled ");
                    var client = clients[newestOrder.userId];
                    if (client)
                    {
                      client.emit("OrderCanceled", {order: newestOrder});
                    }
                    callback(true);
                  }
                  else
                  {
                    callback(false, 'Failed to add cancelled order to [' + order.userId + "/CanceledOrders] object");
                  }
                })
              }
              else
              {
                callback(false, ("Failed to cancel Order [" + order.id + "]"));
              }
          })
        }
        else
        {
          callback(false, ("Failed to cancel user [" + order.userId + "]'s' active order [" + order.id + "]"));
        }
      })
    });
  },
  addCurrency: function (userId, amount, callback)
  {
    Firebase.createFirebaseTransaction("Users/" + userId+"/currency", amount, function(currentAmount){
     // console.log("Comparison? " + currentAmount + " + " + amount);
      return currentAmount + amount >= 0
    }, callback);
  },
  getInstantSellPrice: function (player, market)
  {
    return (player.AdjustedFantasyPercentage/100 * market.marketCap / market.numSharesPerEntity).floor(2)
  },
  logCompletedOrder: function(order, callback){
    console.log("Logging Completed Order: ", getShortOrderString(order, true));
    Firebase.getFirebaseData("Orders/" + order.id, function(newestOrder){
      Firebase.deleteFirebaseKey("Orders/" + order.id, function(deleted){
        if (!deleted)
        {
            callback("Order not successfully deleted");
        }
        Firebase.deleteFirebaseKey("Users/" + order.userId + "/ActiveOrders/" + order.id, function(deletedUserOrder){
          if (!deletedUserOrder)
          {
            callback('User order not successfully deleted');
          }
        });
        Firebase.addToFirebase("Users/" + order.userId + "/CompletedOrders/" + order.id, order, function(orderAdded){
          if (orderAdded)
          {
            var client = clients[order.userId];
            if (client)
            {
              if (newestOrder)
              {
                client.emit("OrderFilled", {order: newestOrder, success: true});
              }
              else
              {
                //Possibly an issue that nnewestOrder is null. But probably it was already deleted.
              }
            }
            callback();
          }
          else
          {
            callback("Order not successfully added to history");
          }
        });
      })
    });
  },
  attemptFillOrder:function(order, callback)
  {
    console.log("Attempting order: ", getShortOrderString(order, true));
      exports[order.orderType].fill(order, function(order, success, err)
      {
        if (success)
        {
          console.log("Order Filled. Order: " + getShortOrderString(order, true));
          exports.logCompletedOrder(order, function(message){
            if (callback)
            {
              callback(order, message == null, message); 
            }
            else if (message)
            {
              console.log(message);
            }
          });
        }
        else
        {
            console.log("Couldn't fill order [" + err + "]: ", getShortOrderString(order, true));
            if (!order.orderType.includes("Limit"))
            {
              exports.cancelOrder(order, err);
            }
        }
    });
  },
  attemptFillAllOrders: function(callback){
    console.log("Attempting to fill all orders.");
    Firebase.getFirebaseData("Orders", function(orders){
      if (orders)
      {
        Object.keys(orders).forEach(function(orderKey){
          exports.attemptFillOrder(orders[orderKey]);
        })
      }
    });
  },
  attemptFillOrders: function(userId){
    this.getOrdersByUser(userId, function(orders){
      if (orders)
      {
        Object.keys(orders).forEach(function(orderKey){
          exports.attemptFillOrder(orders[orderKey]);
        });
      }
    });
  },
  getOrdersByCriteria: function(playerName, criteriaArray, callback)
  {
    exports.getOrdersByPlayer(playerName, function(orders){
      var ret = orders;
      if (criteriaArray != null && criteriaArray.length > 0)
      {
        var queryFunction = queryObjectAsArray;
        for(var key in criteriaArray)
        {
          var criteria = criteriaArray[key];
          ret = queryObjectAsArray(ret, criteria.childPropertyName, criteria.operator, criteria.compareValue);
          queryFunction = queryArray;
        }
      }

      callback(ret);
    })
  },
  getOrdersByPlayer: function(playerName, callback)
  {
    Firebase.getFilteredFirebaseData("Orders", "stockId", playerName, callback)

  },
  getOrdersByUser: function(userId, callback)
  {
    Firebase.getFilteredFirebaseData("Orders", "userId", userId, callback)
  },
  getShortOrderString: function(order,includeId){
    return getShortOrderString(order, includeId);
  },
  MarketSell: require("./Orders/MarketSell.js"),
  LimitSell: require("./Orders/LimitSell.js"),
  Bailout: require("./Orders/Bailout.js"),
  MarketBuy: require("./Orders/MarketBuy.js"),
  LimitBuy: require("./Orders/LimitBuy.js"),
  LeagueBuy: require("./Orders/LeagueBuy.js")
}

function checkOrderRestrictions(order, callback)
{
  exports[order.orderType].isCreationValid(order, function(err){
    callback(err == null, err);
  })
}

function queryObjectAsArray(obj, childPropertyName, operator, compareValue)
{
  return queryArray(_.toArray(obj), childPropertyName, operator, compareValue);
}

function queryArray(values, childPropertyName, operator, compareValue)
{
  var pre = operator == "!=" ? ">" : ".";
  var obj = {x:values};
  var queryString = "$..x[?(@." + childPropertyName + operator + "'" + compareValue + "')]"
 /* console.log("----------------------------------------------------------------------");
  console.log("Object: " + JSON.stringify(obj, null, 2));
  console.log("Query String: " + queryString);*/
  var ret = JSONPath({path: queryString, json: obj});
  /*console.log("Result: ", ret);
  console.log("----------------------------------------------------------------------");
*/
  return ret;
}

function getShortOrderString(order, includeId)
{
  var ret = order.amountOfStock ? order.amountOfStock.toString() : "";
  ret += order.orderType;
  if (order.stockPrice)
  {
    ret += "@$" + order.stockPrice;
  }

  if (includeId)
  {
    ret += "|" + order.id;
  }

  return ret;
}

function fulfillOrder(orderId,callback)
{
  Firebase.getFirebaseData("Orders/" + orderId, function(order){
    if (order != null)
    {
        Firebase.deleteFirebaseKey("Orders/" + orderId);
    }
  })
}

function getBailoutPrice(order, callback)
{
  //hold bailout money sepearte from bank to avoid double spending.
  //after a timeout, that money is also returned
}

function cancelBailout(order, callback)
{
  //return bailout held money to bank
}