
var authenticated = false;
var firebase = require('firebase');

module.exports = exports = {
  isAuthenticated: function()
  {
    return authenticated;
  },
  init: function(callback){
    var config = {
      apiKey: "AIzaSyDIAQWE7g2Jp2s9c6j0mFuE8zDOVka01Fo",
      authDomain: "sportsstocks-123c7.firebaseapp.com",
      databaseURL: "https://sportsstocks-123c7.firebaseio.com/",
      projectId: "sportsstocks-123c7",
      storageBucket: "ssportsstocks-123c7.appspot.com/",
      messagingSenderId: "446704093974"
    };
    firebase.initializeApp(config);

    var auth = firebase.auth().signInWithEmailAndPassword("seebadriarbitrage@gmail.com", "Seebadri08096").catch(
      function(error) 
      {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log("Error Signing in: " + errorCode + " - " + errorMessage);
      }).then(
      function()
      {
        console.log("Logged into firebase");
        authenticated = true;
        callback()
      });
    },

    getFilteredFirebaseData: function(key, childKey, equalityFilter, callback)
    {
      this.actOnFirebaseDbRef(key, function(keyDb){
        keyDb.orderByChild(childKey).equalTo(equalityFilter).once("value", function(snapshot){
          callback(snapshot.val());
        })
      })
    },

    getSortedFirebaseData: function(key, childKey, callback)
    {
      this.actOnFirebaseDbRef(key, function(keyDb){
        keyDb.orderByChild(childKey).once("value", function(snapshot){
          callback(snapshot.val());
        })
      })    
    },

    getFirebaseData: function (key, callback)
    {
      this.actOnFirebaseDbRef(key, function(keyDB){
        keyDB.once("value", function(snapshot)
        {
          callback(snapshot.val());
        });
      });
    },

    addToFirebase: function (key, value, callback)
    {
      this.actOnFirebaseDbRef(key, function(keyDB){
        exports.set(keyDB, value, function(err){
          if (err)
          {
            console.log("Error adding [" + key + "=>" + value + "] to Firebase: ", err);
            if (callback)
              callback(false);
          }
          else if (callback)
          {
            callback(true);
          }
        });
      });
    },

    pushToFirebase: function(key, value, callback)
    {
      this.actOnFirebaseDbRef(key, function(keyDB){
        var pushRef = keyDB.push();
        var newKey = pushRef.key;
        exports.set(pushRef, value, function(err){
          if (err)
          {
            console.log("Error pushing [" + value + "] to key [" + key + "]: ", err);
            if (callback)
            {
              callback(false);
            }
          }
          else if (callback)
          {
            callback(true, newKey);
          }
        });
      });
    },

    createFirebaseTransaction: function (key, valueToAdd, conditional, callback)
    {
      if (valueToAdd == 0) //skip this whole mess if the value to add is 0.
      {
        if (callback)
        {
          callback(true);
        }
        return;
      }
      callback = callback ? callback : function(){};
      var originalVal;
      exports.actOnFirebaseDbRef(key, function(keyDB){
        keyDB.transaction(function(currentVal){
          originalVal = currentVal;
          if (conditional == null || conditional(currentVal))
          {
            return (currentVal || 0) + valueToAdd
          }
          else
          {
            return currentVal;
          }
        }, function(error, committed, snapshot){
          var val = snapshot.val();
          console.log("Firebase Transaction [" + key + " + " + valueToAdd + "] Completed|Error: ", error, "|Commited: ", committed, "|Original Value: ", + originalVal, "|Snapshot: " , val, error ? "|Error: " + error : "");
          if (error) {
            callback(false, "Transaction failed abnormally: " + error);
          } else if (!committed) {
            callback(false, "Transaction aborted");
          } else {
            callback(val != originalVal);
          }
        });
      });
    },

    deleteFirebaseKey: function (key, callback)
    {
      exports.addToFirebase(key, null, callback)
    },
    actOnFirebaseDbRef: function(key, callback)
    {
      if (!authenticated)
      {
        setTimeout(function(){exports.actOnFirebaseDbRef(key, callback);}, 5000);
        return;
      }


      var cleanKey = getCleanKey(key);
      var keyDB = firebase.database().ref(getCleanKey(key));
      callback(keyDB);
    },
    set: function(dbRef, value, callback)
    {
      value = getCleanValue(value);
      dbRef.set(value, function(err){
        if (err)
        {
        }
        if (callback)
        {
          callback(err);
        }
        else if (err)
        {
          console.log("Error Setting [" + value + "] to [" + dbRef.key + ']: ', err);
          console.trace();
        }
      }) 
    }
}

function getCleanValue(value)
{
  value = cloneDeep(value)
  value = cleanKeys(value);
  value = cleanObject(value);
  return value;
}


function getCleanKey(key)
{
  key = key.replaceAll(".", "%2E");
  key = key.replaceAll("#", "%35");
  return key;
}

function cleanKeys(obj)
{//, stack) {
   for (var property in obj) 
   {
     if (obj.hasOwnProperty(property)) {
       if (typeof obj[property] == "object") 
       {
           cleanKeys(obj[property]);//, stack + '.' + property);
       }
      var cleanKey = getCleanKey(property);
      if (cleanKey != property)
      {
       obj[cleanKey] = obj[property];
       delete(obj[property]);
      }
    }
   }
  return obj;
} 

 function cleanObject(obj)
 {
  var ret = obj;
  for (var k in obj)
  {
    if (typeof obj[k] == "object" && obj[k] !== null)
      ret[k] = cleanObject(obj[k]);
    else if (obj[k] == null || obj[k] !== obj[k])
    {
      ret[k] = "NULL";
    }    
  }

  return ret;
}