var fs = require("fs");
const https = require('https');

var Firebase;
var yearStart = new Date().getFullYear();
var yearEnd = yearStart;
if (new Date().getMonth() > 7)
{
  yearEnd += 1;
}
else
{
  yearStart -= 1;
}

function readFile(path)
{
  return fs.readFileSync(path, "utf-8");
}

module.exports = exports = {
	init: function(firebase){
		Firebase = firebase;
	},
	getAskPrice: function(playerName, callback){
		Firebase.getFirebaseData("League/" + playerName + "/AdjustedFantasyPercentage", function(fantasyPercentage){
			Firebase.getFirebaseData("Market", function(market){
				if (!fantasyPercentage || !market)
				{
					callback(null);
				}
				else
				{
					callback((fantasyPercentage/100 * market.marketCap / market.numSharesPerEntity).floor(2))
				}
			});
		});
	},

	getBidPrice: function(playerName, callback){
		exports.getAskPrice(playerName, function(askPrice){
			Firebase.getFirebaseData("Market/instantSellPercentage", function(leaguePercentage){
				if (!askPrice || !leaguePercentage)
				{
					callback(null);
				}
				else
				{
					Firebase.getFirebaseData("League/" + playerName + "/DividendsPerShare", function(dividendsPerShare){
						if (!dividendsPerShare)
						{
							dividendsPerShare = 0;
						}

						callback(((askPrice-dividendsPerShare) * leaguePercentage / 100).floor(2));
					});
				}
			});
		});
	},
	
	grabSeasonStats: function (callback, reset)
	{
	  console.log("Updating League...");
	  var thisSeasonString = yearStart.toString() + "-" + yearEnd.toString().substr(-2);
	  var lastSeasonString = (yearStart-1).toString() + "-" + (yearEnd - 1).toString().substr(-2);
	  var twoSeasonsAgoString = (yearStart-2).toString() + "-" + (yearEnd - 2).toString().substr(-2);
	  Firebase.getFirebaseData("Market", function(market)
	  {
	    Firebase.getFirebaseData("League", function(oldLeague){
	      getLeague(thisSeasonString, false, market, function(league){
	          Object.keys(league).forEach(function(name){

	            if (oldLeague[name])
	            {
	              if (oldLeague[name].Stocks)
	              {
	              	if (reset == null || reset == false)
	              	{
	              	  	league[name].Stocks= oldLeague[name].Stocks;
		            }
	                if (league[name].Stocks < 0)
	                {
	                  console.log(name + " has negative stocks: [" + league[name].Stocks) + "]";
	                  league[name].Stocks = NaN;
	                }
	              }

	              if (oldLeague[name]["FantasyPointsPercentageHistory"])
	              {
	                league[name]["FantasyPointsPercentageHistory"] = oldLeague[name]["FantasyPointsPercentageHistory"];
	              }

	              if (oldLeague[name]["FantasyPointsHistory"])
	              {
	              	league[name]["FantasyPointsHistory"] = oldLeague[name]["FantasyPointsHistory"];
	              }

	              if (oldLeague[name].PotentialDividends)
	              {
	              	league[name].PotentialDividends = oldLeague[name].PotentialDividends;
	              }
	            }

	            league[name]["FantasyPointsPercentageHistory"][getDateKey()] = league[name].FantasyPointsPercentage;
	            league[name]["FantasyPointsHistory"][getDateKey()] = league[name].NBA_FANTASY_PTS
	          });
	            
	          getLeague(lastSeasonString, true, market, function(previousPlayoffs) {
	            getLeague(lastSeasonString, false, market, function(lastYearsLeague) {
	              getLeague(twoSeasonsAgoString, true, market, function(twoYearsAgoPlayoffs) {
	                getLeague(twoSeasonsAgoString, false, market, function(twoYearsAgoLeague) {
	                  var leagues = [league, previousPlayoffs,lastYearsLeague, twoYearsAgoPlayoffs, twoYearsAgoLeague];
	                  var totalScore = 0;
	                  Object.keys(league).forEach(function(name){
	                    var numGames = 0;
	                    var scoreDict = []
	                    for(var i = 0; i < leagues.length; i++)
	                    {
	                      if (leagues[i][name])
	                      {
	                        var thisPlayer = leagues[i][name];
	                        scoreDict.push({GP: thisPlayer.GP, Score: thisPlayer.NBA_FANTASY_PTS, index: i});
	                        numGames += thisPlayer.GP;
	                        if (numGames >= market.numGamesForPricing)
	                        {
	                          break;
	                        }
	                      }
	                    }

	                    var finalScore = 0;
	                    var totalGamesToAverage = numGames > market.numGamesForPricing ? market.numGamesForPricing : numGames;
	                    numGames = 0;
	                    var i = 0;
	                    while(numGames < totalGamesToAverage && i < scoreDict.length)
	                    {
	                      if (numGames + scoreDict[i].GP > totalGamesToAverage)
	                      {
	                        scoreDict[i].GP = totalGamesToAverage - numGames;
	                      }

	                      var adjustedScore = (scoreDict[i].Score * scoreDict[i].GP / totalGamesToAverage);
	                      finalScore += adjustedScore
	                      numGames += scoreDict[i].GP;
	                      i++;
	                    }

	                    totalScore += finalScore;
	                    league[name].AdjustedFantasyScore = finalScore;
	                  });

	                  Object.keys(league).forEach(function(playerName){
	                    league[playerName].AdjustedFantasyPercentage = league[playerName].AdjustedFantasyScore / totalScore * 100
	                    var lastPlayer = lastYearsLeague[playerName];
	                    var value = lastPlayer ? lastPlayer.FantasyPointsPercentage : 0;
	                    //console.log(playerName + ": " + value);
	                    league[playerName].PreviousFantasyPointsPercentage =  value
	                  });

 	                  Firebase.addToFirebase("League", league);
	                  Firebase.addToFirebase("Market/lastUpdateLeagueTime", new Date().getTime())
	                  console.log("Completed league update stats update");
	                  if (callback)
	                  {
	                  	callback();
	                  }
	                });
	              });
	            });
	          });
	        });
	    });
	  });
	}
}

function getLeague (year, playoffs, market, callback)
{
	var seasonType = playoffs ? "Playoffs":"Regular+Season";
	var dir = "./data";
	if (!fs.existsSync(dir)){
		fs.mkdirSync(dir);
	}
	var jsonFile = dir + "/" + seasonType.replace("+", "") + "_" + year + ".json";
	console.log("Downloading: " + jsonFile);
	var file = fs.createWriteStream(jsonFile);
	var request = https.get('https://stats.nba.com/stats/leaguedashplayerstats?College=&Conference=&Country=&DateFrom=&DateTo=&Division=&DraftPick=&DraftYear=&GameScope=&GameSegment=&Height=&LastNGames=0&LeagueID=00&Location=&MeasureType=Base&Month=0&OpponentTeamID=0&Outcome=&PORound=0&PaceAdjust=N&PerMode=Totals&Period=0&PlayerExperience=&PlayerPosition=&PlusMinus=N&Rank=N&Season='+ year + '&SeasonSegment=&SeasonType=' + seasonType + '&ShotClockRange=&StarterBench=&TeamID=0&VsConference=&VsDivision=&Weight=', function(response) {
	  response.pipe(file);
	  response.on('end', function(){
	  	console.log("Stats for " + year + "[Playoffs: " + playoffs + "] downloaded");
	  	callback(parseData(jsonFile, market));
	  })
	});

	
//	console.log("Retrieving " + jsonFile);
//	execute("curl -o " + jsonFile + ' "https://stats.nba.com/stats/leaguedashplayerstats?College=&Conference=&Country=&DateFrom=&DateTo=&Division=&DraftPick=&DraftYear=&GameScope=&GameSegment=&Height=&LastNGames=0&LeagueID=00&Location=&MeasureType=Base&Month=0&OpponentTeamID=0&Outcome=&PORound=0&PaceAdjust=N&PerMode=Totals&Period=0&PlayerExperience=&PlayerPosition=&PlusMinus=N&Rank=N&Season='+ year + '&SeasonSegment=&SeasonType=' + seasonType + '&ShotClockRange=&StarterBench=&TeamID=0&VsConference=&VsDivision=&Weight=" -H "Accept-Language: en-US,en;q=0.9" -H "Upgrade-Insecure-Requests: 1" -H "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*///*;q=0.8" -H "Cache-Control: max-age=0" -H "Cookie: s_fid=22E02F422F41699C-0ACC9F6C8DD7111B; ug=5a69785a029c8d0a3c21441f0a04b7c6; s_vi=[CS]v1|2D34BC2D85033EA2-4000118920008BFE[CE]; _ga=GA1.2.1791957563.1516902098; _gid=GA1.2.91385368.1517515637; ugs=1" -H "Connection: keep-alive"', function(r){
//		console.log("Did i get here with jsonFile " + jsonFile);
//		callback(parseData(jsonFile, market));
//	});
}

function parseData(responseFile, market)
{
  var response = JSON.parse(readFile(responseFile));
  var leagueStatsResponse = response.resultSets[0];
  var league = {}
  var totalPoints = 0;
  leagueStatsResponse.rowSet.forEach(function(row){
    var name = row[1];
    league[name] = {};
    var i = 0;
    leagueStatsResponse.headers.forEach(function(header)
    { 
      league[name][header] = row[i];
      i++
    });

    league[name]["FantasyPointsPercentageHistory"] = {};
    league[name]["FantasyPointsHistory"] = {};
    league[name]["Stocks"] = market.numSharesPerEntity;
    totalPoints += league[name].NBA_FANTASY_PTS;

  });

  Object.keys(league).forEach(function(playerName){
    league[playerName].FantasyPointsPercentage = league[playerName].NBA_FANTASY_PTS / totalPoints * 100;
  })
  return league;
}
