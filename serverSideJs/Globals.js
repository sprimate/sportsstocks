//Global Helpers
global.Number.prototype.floor = function(places) {
  return +(Math.floor(this + "e+" + places)  + "e-" + places);
}

global.Number.prototype.round = function(places) {
  return +(Math.round(this + "e+" + places)  + "e-" + places);
}

global.String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

const { exec } = require('child_process');
global.execute = function(command, callback){
  console.log(command)
  exec(command, function(error, stdout, stderr){ callback(stdout); });
};

global.timestamp = function(){
  return Date.now();
}

global.getDateKey = function(date)
{
  var date = date ? date : new Date();
  return date.getMonth() + 1 + "|" + date.getDate() + "|" + date.getFullYear();//new Date().toString();
}

global.getDateFromDateKey = function(dateKey){
	var split = dateKey.split("|");
	return new Date(parseInt(split[2]), parseInt(split[0])-1, parseInt(split[1]), 0, 0, 0, 0);
}

global.getDateFromToday = function(){
	return getDateFromDateKey(getDateKey());
}

global.shuffle = function(array)
{
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
};

//inclusive
global.random = function(min, max)
{
	return Math.floor(Math.random() * (max-min+1) + min);
}

global.randomFloat = function(min, max)
{
	return Math.random() * (max - min) + min;
}

global.cloneDeep = require("clone-deep");

global._ = require('lodash');
global.clients = {};

//Outside Code
global.Firebase = require("./Firebase.js")
global.Orders = require("./Orders.js");
global.League = require("./League.js");
global.Earnings = require("./Earnings.js");
League.init(Firebase);
Orders.init(Firebase, League);