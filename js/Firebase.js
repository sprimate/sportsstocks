//scriptsToLoad = ["https://www.gstatic.com/firebasejs/4.6.2/firebase-app.js", "https://www.gstatic.com/firebasejs/4.6.2/firebase-database.js", "https://www.gstatic.com/firebasejs/4.6.2/firebase-auth.js" ];
//loadScripts(scriptsToLoad, function () {
    //firebase.initializeApp(firebaseConfig);

	//authenticate();
//});

function initFirebase() {
    firebase.auth().onAuthStateChanged(function (user) {
        checkAuthentication();
    });
    checkAuthentication();
}

var onAuthenticated
var authenticated = false;
var authenticationAttempt = 0;
var maxAuthenticationAttempts = 5;
var data = null;
var db;
var firebaseConfig = {
	apiKey: "AIzaSyDIAQWE7g2Jp2s9c6j0mFuE8zDOVka01Fo",
	authDomain: "sportsstocks-123c7.firebaseapp.com",
	databaseURL: "https://sportsstocks-123c7.firebaseio.com/",
	projectId: "sportsstocks-123c7",
	storageBucket: "sportsstocks-123c7.appspot.com/",
	messagingSenderId: "446704093974"
};

function checkAuthentication() {
    firebaseUser = firebase.auth().currentUser;
    authenticated = firebaseUser !== null;
    if (authenticated)
    {
        db = firebase.database();
        if (onAuthenticated != null)
        {
        	onAuthenticated(firebaseUser);
        }
    }
}

function getUserData(key, useThisCallbackInsteadOfDataVariable) {
    if (!authenticated) {
        console.log("Cannot get user data - Authentication not detected. Please login");
    }
    else {
        getData(getUserDataPrefix() + key, true, useThisCallbackInsteadOfDataVariable);
    }
}

var onDataUpdated;
function getData(key, keyIsNotDate, useThisCallbackInsteadOfDataVariable)
{
 //   console.log("Getting: " + key);
	if (!authenticated)
	{
		setTimeout(function(){getData(key, keyIsNotDate, useThisCallbackInsteadOfDataVariable);}, 100);
		return;
	}

    var firebaseKey = null;
	if (keyIsNotDate)
	{
		firebaseKey = key;
	}
	else
	{
		firebaseKey = (key.getMonth() + 1) + "|" + key.getDate() + "|" + key.getFullYear();//new Date().toString();
	}

	console.log("Getting key: " + firebaseKey);
	var ref = key ? db.ref(firebaseKey) : db.ref();
	ref.once("value", function(snapshot) 
	{
		console.log("Received: " + snapshot, snapshot.val());
		if (useThisCallbackInsteadOfDataVariable)
		{
			useThisCallbackInsteadOfDataVariable(snapshot.val());
		}
		else
		{
			data = snapshot.val();

			if (onDataUpdated)
			{
				onDataUpdated(data);
			}
		}

    }, function (errorObject) {
        console.log("The read failed: " + errorObject);
	});
}

function getUserDataPrefix() {
    return "UserData/" + firebase.auth().currentUser.uid + "/";
}

function setUserData(key, obj, date) {
    if (!authenticated) {
        console.log("Can't set dta because not authenticated: " + key, new Error().stack);
    }
    else {
        setData(getUserDataPrefix() + key, obj, date);
    }
}

function setData(key, obj, date){
	if (date === null)
	{
		date = $("#datepicker").datepicker("getDate");
	}
	else if (date !== false)
	{
		date = date ? date : $("#datepicker").datepicker("getDate");
		var firebaseKey = (date.getMonth() + 1) + "|" + date.getDate() + "|" + date.getFullYear();//new Date().toString();
		firebaseKey += "/" + key;
	}
	else
	{
		firebaseKey = key;
	}
    var ref = db.ref(key);
	ref.set(obj);
}
