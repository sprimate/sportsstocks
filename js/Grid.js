function initJsGrid() {
    loadStyleSheet("https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css");
    loadStyleSheet("https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css");
    loadStyleSheet("https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css");
    loadScript("https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.js");
    var style = document.createElement('style');
    style.innerHTML = '.jsgrid-cell {overflow: hidden;} .jsgrid-header-cell {overflow: hidden;}'
    document.head.appendChild(style); 
}
