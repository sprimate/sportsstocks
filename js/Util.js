Number.prototype.floor = function(places) {
  return +(Math.floor(this + "e+" + places)  + "e-" + places);
}

Number.prototype.round = function(places) {
  return +(Math.round(this + "e+" + places)  + "e-" + places);
}

function loadScripts(scriptsToLoad, callback, dontReverseOrder)
{
	callback = callback ? callback : function(){}
	if (!dontReverseOrder)
	{
		scriptsToLoad.reverse();
	}
	if (scriptsToLoad.length > 0)
	{
		loadScript(scriptsToLoad.pop(), function(){
			if (scriptsToLoad.length > 0)
			{
				loadScripts(scriptsToLoad, callback, true);
			}
			else
			{
				callback();
			}
		});
	}
}

function loadScript(url, callback)
{
	var script = document.createElement('script');
	script.onload = function () {
		if (callback)
		{
			callback();
		}
	};
	script.src = url;

	document.head.appendChild(script); //or something of the likes
}

function loadStyleSheet(url)
{
	var sheet = document.createElement('link');
	sheet.rel = "stylesheet"
	sheet.href = url;
	document.head.appendChild(sheet); //or something of the likes
}

//the price in which you can sell a player to the league instantly
function getLeagueBuyPrice(playerName, league, market)
{
	return (getLeagueSellPrice(playerName, league, market) * market.instantSellPercentage / 100).floor(2);
}

//The price in which you can purchase a player from the league
function getLeagueSellPrice(playerName, league, market)
{
	return (league[playerName].AdjustedFantasyPercentage/100 * market.marketCap / market.numSharesPerEntity).floor(2)
}

function fixDBStrings(str)
{
	str = str.replace("%2E", ".");
	return str;
}

var modal; //initialized in the initUtil function
var modalContent;
function initUtil() 
{

}

function displayModal(content, onclose)
{
	onModalHide = onclose;
	var modalInitialized = true;
	if (modal == null)
	{
		modal = document.getElementById("modal");
		modalInitialized = false;
	}

	if (modal == null)
	{
		modal = document.createElement("div");
		document.body.appendChild(modal);
	}

	if (!modalInitialized && modal != null)
	{
		modal.className = "modal";
		var div = document.createElement("div");
		div.className = "modal-internal";
		var span = document.createElement("span");
		span.innerHTML = "&times;";
		span.className = 'close';
		span.onclick = function() {
		  hideModal();
		}
		div.appendChild(span);
		modalContent = document.createElement("div");
		modalContent.id = "modal-content";
		modalContent.className = "modal-content";
		div.appendChild(modalContent);
		modal.appendChild(div);

		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
		  if (event.target == modal) {
		  	hideModal();
		  }
		}
	}

	console.log("Display modal with content: " , content);
	if (content != null && modalContent != null)
	{
		console.log("modalContent: ", modalContent);
		if (typeof content === 'string' || content instanceof String)
		{
			modalContent.innerHTML = content;
		}
		else
		{
			modalContent.innerHTML = content.innerHTML;
		}
	}
	modal.style.display = "block";
}

var onModalHide;
function hideModal()
{
	modalPlayer = null;
    modal.style.display = "none";
    if (onModalHide)
    {
    	onModalHide();
    	onModalHide = null;
    }
}

var modalPlayer;
function displayPurchaseModal(player, league, market)
{
	modalPlayer = player;
	var ret = "Player: " + player.Name + "</br>";
	var leagueSellPrice =	getLeagueSellPrice(player.Name, league, market)
	ret += "Approximate League Stock Price: " + leagueSellPrice +"</br>";
	ret += "Projected Yearly Earnings: " + (league[player.Name].FantasyPointsPercentage/100 * market.marketCap / market.numSharesPerEntity).floor(2) + "</br>"
	ret += "<h2> Purchase Stock</h2>";
	/*if (player.Owned != player["Stocks Owned"])
	{
		ret += "Num Stocks Waiting for Order Completion: " + (parseInt(player["Stocks Owned"]) - parseInt(player.Owned)) + "</br>";
	}*/
	var selectId = "numSharesToBuy"
	ret += "Num Stocks to Buy<select id='" + selectId + "'>";
	for(var i = 1; i <= market.numSharesPerEntity; i++)
	{
		ret += "<option>" + i + "</option>"
	} 
	ret += "</select></br>"
	//Market
	ret += "<hr><h3>Market Purchase</h3><div>A Market purchase will attempt to buy the selected amount of stock on the open market at the best available price</div>"
	ret += "<button onclick='marketBuy();'>Create Market Purchase Order</button>"

	//Limit
	ret += "<hr><h3>Limit Purchase</h3><div>A Limit purchase will attempt to buy the requested stock on the open market at the given price.</div>"
	ret += "Price: <input id='sellPrice' type='number' value='" + (leagueSellPrice - 0.01).round(2) + "'></br>";
	ret += "<button onclick='limitBuy();'>Create Limit Purchase Order</button>"

	//Bailout
	ret += "<hr><h3>League Purchase</h3><div>A league purchase will immediately purchase the requested stock at the guaranteed league price..</div>"

	ret += "Estimated Price: " + getLeagueSellPrice(player.Name, league, market) + "<button onclick=\"leagueBuy(" + leagueSellPrice + ");\">Execute League Purchase</button></br>";

	displayModal(ret);
	//displayModal("Clicked on " + player.Name +"</br> Or something </br> like that");	
}

function leagueBuy(price)
{
	createOrder("LeagueBuy", modalPlayer, getValueFromSelect("numSharesToBuy"), price);
}

function limitBuy()
{
	var price = parseFloat($("#sellPrice").val()).floor(2)
	createOrder("LimitBuy", modalPlayer, getValueFromSelect("numSharesToBuy"), price);
}

function marketBuy()
{
	createOrder("MarketBuy", modalPlayer, getValueFromSelect("numSharesToBuy"));
}

function displaySellModal(player)
{
	modalPlayer = player;
	var ret = "Player: " + player.Name + "</br>";		
	ret += "Approximate Market Stock Price: " + getLeagueSellPrice(player.Name, league, market) +"</br>";
	ret += "Projected Yearly Earnings: " + (league[player.Name].FantasyPointsPercentage/100 * market.marketCap / market.numSharesPerEntity).floor(2) + "</br>"
	ret += "Stocks Owned: " + player["Stocks Owned"] + "</br>";
	ret += "<h2> Sell Stock</h2>";
	/*if (player.Owned != player["Stocks Owned"])
	{
		ret += "Num Stocks Waiting for Order Completion: " + (parseInt(player["Stocks Owned"]) - parseInt(player.Owned)) + "</br>";
	}*/
	ret += "Num Stocks to Sell<select id='numSharesToSell'>";
	console.log(player);
	for(var i = 1; i <= player["Stocks Owned"]; i++)
	{
		ret += "<option>" + i + "</option>"
	} 
	ret += "</select></br>"
	//Market
	ret += "<h3>Market Sell</h3><div>A Market sale will attempt to sell your selected stock on the open market at the best available price</div>"
	ret += "<button onclick='marketSell();'>Create Market Sell Order</button>"

	//Limit
	ret += "<h3>Limit Sell</h3><div>A Limit sale will attempt to sell your stock on the open market at the given price.</div>"
	ret += "Price: <input id='sellPrice' type='number' value='" + (getLeagueSellPrice(player.Name, league, market) - 0.01) + "'></br>";
	ret += "<button onclick='limitSell();'>Create Limit Sell Order</button>"

	//Bailout
	ret += "<h3>League Bailout</h3><div>A Bailout is available if you would like to dump your stock for a fraction of what it's current purchase price is. You will never make a profit on a bailout, but it can be used to mitigate losses.</div>"
	ret += "Estimated Price: " + getLeagueBuyPrice(player.Name, league, market) + "<button onclick=\"bailout();\">Request Bailout</button></br>";


	displayModal(ret);
}

function bailout()
{
	createOrder("Bailout", modalPlayer, getValueFromSelect("numSharesToSell"), getLeagueBuyPrice(modalPlayer.Name, league, market) );
}

function limitSell()
{
	var price = parseFloat(getValueFromSelect("sellPrice")).floor(2)
	createOrder("LimitSell", modalPlayer, getValueFromSelect("numSharesToSell"), price);
}

function marketSell()
{
	createOrder("MarketSell", modalPlayer, getValueFromSelect("numSharesToSell"));
}

function createOrder(_orderType, player, _amountOfStock, price)
{
	getData("Users/" + firebaseUser.uid + "/currency", true, function(currency){
		//console.log("Num Shares: " + getNumSharesToSell() + " because ", $("#numSharesToSell") );
		var data = {
			userId: firebaseUser.uid,
			amountOfStock: _amountOfStock,
			stockId: player.Name,
			stockPrice: price,
			orderType: _orderType
		}
		console.log(_orderType);
		var totalOrderPrice = (data.stockPrice * data.amountOfStock).floor(2);
		var newCurrency = _orderType.toLowerCase().includes("buy") ? (currency - totalOrderPrice).floor(2) : (currency + totalOrderPrice).floor(2);
		if (window.confirm("Are you sure you want to create a [" + _orderType + "] order for " + data.amountOfStock + " shares of '" + data.stockId + "' at $" + data.stockPrice + " for a total of $" + totalOrderPrice + "? This will bring your balance from $" + currency.floor(2) + " to $" + newCurrency ))
		{
			socket.emit("CreateOrder", data);
		}
	});
}

function getNextPayoutDate(market)
{
	return new Date(market.nextEarningsPaidTime);
}

function getNextPayoutAmount(market)
{
	var payoutsLeftBeforeReset =getPayoutsLeftBeforeReset(market)
	console.log(payoutsLeftBeforeReset, market.bank);
	if (payoutsLeftBeforeReset <= 0)
	{
		return 0;
	}
	
	return (market.bank /payoutsLeftBeforeReset).floor(2);
}

function getPayoutsLeftBeforeReset(market)
{
	var resetDate = new Date(market.earningsResetDate);

	var targetTime = 1 / (market.targetDividendYield/100);
	var toAdd = targetTime >= 1 ? targetTime - 1 : -(1-targetTime);
	toAdd *= 365.25;//how many days to add or subtract to the target date to match the target dividend yield.
	resetDate.setDate(resetDate.getDate() + toAdd);

	var nowDate = new Date();
	var differenceInDays = (resetDate - nowDate) / (1000 * 3600 * 24); 
	return  Math.floor(differenceInDays / market.earningsPayoutFrequencyInDays)
}
function refreshPage()
{
	location.reload(false);
}

function getValueFromSelect(id)
{
	return document.getElementById(id).value;
}

function onConnected()
{
	console.log("Connected")
	var ret = firebaseUser ? firebaseUser.uid : null;
	console.log("Connected " + ret);
   	socket.emit('Joined', ret );
}

var socket = io()//io.connect("http://localhost:5000");
socket.on('connect', onConnected);
socket.on('reconnect', onConnected);

socket.on("OrderCreationResponse", function(result){
	console.log("REceived: ", result)
	var prefix = "<pre>Creation of " + result.data.orderType + " order ";
	var suffix = "\n" + JSON.stringify(result.data, null, 2) + "</pre>"
	if (result.success)
	{
		displayModal(prefix + "Successful!" + suffix, refreshPage);
	}
	else
	{
		displayModal(prefix + "Unsucessful: " + result.message + suffix, refreshPage);
	}
});

socket.on("OrderFilled", function(result){
	displayModal("<pre>Order Fulfilled:\n" + JSON.stringify(result.order, null, 2) + "</pre>", refreshPage);
});

socket.on("OrderCanceled", function(result){
	displayModal("<pre>Order Canceled:\n" + JSON.stringify(result.order, null, 2) + "</pre>", refreshPage);
});